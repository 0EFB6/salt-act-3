import Head from "next/head";

const SuccessPage = () => {
    const handleBackButtonClick = () => {
        // Navigate back to the index page
        window.location.href = "/";
    };

    return (
        <>
            <Head>
                <title>Login Successful Page</title>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="style.css" />
            </Head>
            <main id="success-main">
                <h1>解锁成功！</h1>
                <input type="submit" value="返回" id="back-button" onClick={handleBackButtonClick} />
            </main>
        </>
    );
};

export default SuccessPage;
